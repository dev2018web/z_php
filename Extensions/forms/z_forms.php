<?php 

class Form
{
	
	public function createInput($nameField="", $type="", $class="", $nameInput){
		return new Input($nameField, $type, $class, $nameInput);
	}

}

class Input
{
	private $type;
	private $class;
	private $nameField;
	private $nameInput;


	public function __construct($nameField="", $type="", $class="", $nameInput)
	{	
		$this->nameField = $nameField;
		$this->type = $type;
		$this->class = $class;
		$this->nameInput = $nameInput;

		$this->create();
	}

	public function create(){
		if ($this->type == "") {
			$this->type = "text";
		}
		if ($this->class == "") {
			$this->class = "class";
		}
		if ($this->nameField != "") {
			$label = "<label for='test'>". $this->nameField ."</label>";
		}else{
			$label = "";
		}

		$input = "<input type=". $this->type ." class=". $this->class ." name=". $this->nameInput .">";
		
		echo $label;
		echo $input;
	}
}

 ?>