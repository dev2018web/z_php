<?php 
require 'environment.php';
session_start();

$config = array();


if (ENVIRONMENT == "development") {
	define('BASE_URL', 'https://fram.test');
	$config['name'] = "pdf_tube";	
	$config['host'] = "localhost";
	$config['user'] = "root";
	$config['pass'] = "";
}else{
	define('BASE_URL', 'test.mvc');
	$config['name'] = "pdf_tube";	
	$config['host'] = "localhost";
	$config['user'] = "root";
	$config['pass'] = "";
}

global $db;
try{
	$db = new PDO("mysql:dbname=". $config['name'] .";host=".$config['host'].";", $config['user'], $config['pass']);

}catch(PDOException $e){
	die($e->getMessage());
}



 ?>