<?php 

class Core
{	
	
	public function run()
	{
		$url = "/";
		$params = array();

		if (isset($_GET['url']) && !empty($_GET['url'])){
			$url .= $_GET['url'];
			$url = explode("/", $url);
			array_shift($url);

			if (isset($url[0]) && !empty($url[0])) {
				$currentController = $url[0] . 'Controller';
				array_shift($url);

				if (isset($url[0]) && !empty($url['0'])) {
					$currentAction = $url[0] . "Action";
					array_shift($url);

					if (isset($url[0]) && $url[0] != "/" && !empty($url[0])) {
						$params = $url;
					}
				}else{
					$currentAction = "indexAction";
				}


			
			}
		}else{
				$currentController = 'homeController';
				$currentAction = 'indexAction';
		}

		if (!file_exists('../controllers/'.$currentController.'.php') || !method_exists(new $currentController, $currentAction)) {
			$currentController = 'notFoundController';
			$currentAction = 'indexAction';


		}



		$c = new $currentController();
		call_user_func(array($c, $currentAction), $params);



		
	}
}


 ?>