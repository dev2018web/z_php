<?php 
require '../Extensions/forms/z_forms.php';

class AdminController extends controller
{
	
	public function indexAction()
	{	
		/*-------------IF THE SESSION IS SETTED---------------------
		  ----------------------------------------------------------*/

		if (isset($_SESSION['login']) && !empty($_SESSION['login'])) {
			$dados = array(
				'form' => new Form()
			);
			$this->loadView($dados);
		}else{
			/*--IF THE SESSION IS NOT SETTED IT WILL CALL THE ACTION LOGIN--
		      --------------------------------------------------------------*/
 			$this->loginAction();
		}
	}

	public function loginAction()
	{	
		$dados = array(
			'form' => new Form()
		);

		if (!isset($_SESSION['login']) && empty($_SESSION['login'])) {
			$this->loadView($dados, "login");

			if (isset($_POST['email']) && !empty($_POST['email'])) {
				if ($_POST['password'] && !empty($_POST['password'])) {
					$login = new Login("index");
					$login->logar();
				}else{
					/*-------------------RESPONSE AS JSON-----------------------
				  	  ----------------------------------------------------------*/
				  	  echo "Você precisa digitar seu e-mail e senha";
				}
			}
		}else{
			header("Location: " . "http://fram.test/admin/index");
		}
	}

	public function logoutAction()
	{	
		unset($_SESSION['login']);

		header("Location: " . "http://fram.test/admin/login");
	}
}

 ?>