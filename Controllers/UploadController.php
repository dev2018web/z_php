<?php 
class UploadController extends controller
{

	public function indexAction()
	{
		$dados = array();

		$this->loadView($dados);
	}

	public function imageSave()
	{	
		/*------------VERIFYNG IF THE FILE IS SETED-----------------
		  ----------------------------------------------------------*/
		if (isset($_FILES) && !empty($_FILES['file'])) {

			$file = $_FILES['file']['tmp_name'];

			$typeArray = explode("/", mime_content_type($file));
			$extension = end($typeArray);

			/*------------------------FORMATS ALLOWED-----------------------
			  --------------------------------------------------------------*/
			$formatsAllowed = array("jpg", "png", "jpeg", "gif");
			/*-----------VERIFYING IF THE FILE FORMAT IS ALLOWED------------
			  --------------------------------------------------------------*/
			if (in_array($extension, $formatsAllowed)) {
				$newName = md5(uniqid() . rand(0, 9999) . time()) . "." . $extension;

				move_uploaded_file($file, "../uploads/" . $newName);

				/*-------------------RESPONSE AS JSON-----------------------
				  ----------------------------------------------------------*/
				  echo "Arquivo enviado com sucesso!";

			}else{
				/*-------------------RESPONSE AS JSON-----------------------
				  ----------------------------------------------------------*/
				echo "Arquivo inválido";

			}

			$nome = md5(uniqid() . rand(0, 99999) . time());

			move_uploaded_file($_FILES['file']['tmp_name'], '../uploads/photos' . $_FILES['file']['name']);
		}else{
			/*-------------------RESPONSE AS JSON-----------------------
				  ----------------------------------------------------------*/
			echo "Arquivo está vazio";
		}
	}


	public function documentSave()
	{	
		/*------------VERIFYNG IF THE FILE IS SETED-----------------
		  ----------------------------------------------------------*/
		if (isset($_FILES) && !empty($_FILES['file'])) {

			$file = $_FILES['file']['tmp_name'];

			$typeArray = explode("/", mime_content_type($file));
			$extension = end($typeArray);

			/*------------------------FORMATS ALLOWED-----------------------
			  --------------------------------------------------------------*/
			$formatsAllowed = array("pdf", "doc", "odt", "pptx");
			/*-----------VERIFYING IF THE FILE FORMAT IS ALLOWED------------
			  --------------------------------------------------------------*/
			if (in_array($extension, $formatsAllowed)) {
				$newName = md5(uniqid() . rand(0, 9999) . time()) . "." . $extension;

				move_uploaded_file($file, "../uploads/documents/" . $newName);

				/*-------------------RESPONSE AS JSON-----------------------
				  ----------------------------------------------------------*/
				  echo "Arquivo enviado com sucesso!";

			}else{
				/*-------------------RESPONSE AS JSON-----------------------
				  ----------------------------------------------------------*/
				echo "Arquivo inválido";

			}

			$nome = md5(uniqid() . rand(0, 99999) . time());

			move_uploaded_file($_FILES['file']['tmp_name'], '../uploads/' . $_FILES['file']['name']);
		}else{
			/*-------------------RESPONSE AS JSON-----------------------
				  ----------------------------------------------------------*/
			echo "Arquivo está vazio";
		}
	}

}

 ?>