<?php

class Login{
	private $redirectTo;

	public function __construct($pageRedirect){
		$this->redirectTo = $pageRedirect;
	}	

	public function getParams()
	{
		if (isset($_POST['email'])) {
			if (ValidateData::isEmail($_POST['email']) == 1) {
				return $_POST;
			}	
		}
	}

	private function query()
	{	
		global $db;


		$post = $this->getParams();

		$email = addslashes($post['email']);
		$password = addslashes($post['password']);

		$sql = "SELECT * FROM users WHERE email = :email";
		$sql = $db->prepare($sql);
		$sql->bindValue(":email", $email);

		$sql->execute();

		if ($sql->rowCount() > 0) {
			$data = $sql->fetch();

			if(password_verify($password, $data['pass'])){
				$_SESSION['login'] = $data;
			}

			return $_SESSION['login'];
		}
	}
	
	public function logar()
	{	
		/*------IF THE NUMBER OF ATTEMPS BE BIGGER THAN THREEE------
		  ---------IT BLOCKS THE ATTEMPS FOR THREE MINUTES----------*/
		if ($_SESSION['attemps'] > 3) {
			echo "Número de tentativas esgotado. Aguarde 3 minnutos";

		    if ($this->saveTimeError() > 180) {
		    	$this->zeroAttributes();
		    }

		}else{
			$this->query();
			if (isset($_SESSION['login']) && !empty($_SESSION['login']) && $_SESSION['login'] != "") {
				$this->zeroAttributes();
				/*-------------------RESPONSE AS JSON-----------------------
			  	  ----------------------------------------------------------*/
			  	  header("Location: " . $this->redirectTo);
			}else{
				/*-------------------RESPONSE AS JSON-----------------------
			  	  ----------------------------------------------------------*/
				echo "Errou";

				$time = time();

				if ($this->countAttemps() > 2) {
					$_SESSION['lastError'] = $time;
				}
			}
		}
	}

	/*--------ATTRIBUTS BECOME ZERO-------
	  ------------------------------------*/
	private function zeroAttributes(){
		$_SESSION['lastError'] = 0;
		$_SESSION['attemps'] = 0;
	}

	/*------IT RETURNS THE NUMBER OF ATTEMPS------
	  --------------------------------------------*/
	private function countAttemps(){
		return $_SESSION['attemps'] += 1;	
	}

	/*--------SAVE DE THIME OF THE ERROR----------
	  --------------------------------------------*/
	private function saveTimeError(){
		$currentTime = time();

		return $tempoRestante = $currentTime - $_SESSION['lastError'];

	}


}

 ?>