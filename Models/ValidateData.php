<?php 

interface dataInterface{
	public static function isEmail($email);
	public static function isInt($int);
	public static function isFloat($float);
	public static function isUrl($url);
}

class ValidateData implements dataInterface
{
	public static $email;
	public static $int;
	public static $float;
	public static $url;
	public static $status;


	public static function isEmail($email="")
	{
		self::$email = $email;
		self::checkMail();

		return self::$status;

	}
	private static function checkMail()
	{
		self::$status="";
		if (filter_var(self::$email, FILTER_VALIDATE_EMAIL)) {
			self::$status = 1;
		}else{
			self::$status = 0;
		}
	}

	public static function isInt($int="")
	{
		self::$int = $int;
		self::checkInt();

		return self::$status;
	}
	private static function checkInt()
	{
		self::$status="";
		if (filter_var(self::$int, FILTER_VALIDATE_INT)) {
			self::$status = 1;
		}else{
			self::$status = 0;
		}
	}

	public static function isFloat($float="")
	{
		self::$float = $float;
		self::checkFloat();

		return self::$status;
	}
	private static function checkFloat()
	{
		self::$status="";
		if (filter_var(self::$float, FILTER_VALIDATE_FLOAT)) {
			self::$status = 1;
		}else{
			self::$status = 0;
		}
	}

	public static function isUrl($url="")
	{
		self::$url = $url;
		self::checkUrl();

		return self::$status;
	}
	private static function checkUrl()
	{
		self::$status="";
		if (filter_var(self::$url, FILTER_VALIDATE_URL)) {
			self::$status = 1;
		}else{
			self::$status = 0;
		}

	}

}




 ?>