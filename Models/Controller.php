<?php 

class controller{

	public function loadview($dados="", $action=""){
		
		$folder = "";
		$file = "";

		if ($_GET != "" && !empty($_GET)) {
			$url = explode("/", $_GET['url']);
		}else{
			$url = array();
		}

		if (isset($url[0]) && $url[0] != "" && !empty($url[0]) && $url[0] != "home") {
			$folder = $url[0];

			array_shift($url);

			if (empty($action) || $action == "") {
				if (isset($url[0]) && !empty($url[0])) {
					$file = $url[0];
				}else{
					$file = "index";
				}
			}else{
				$file = $action;
			}

			
		}else{
			$folder = "";
			$file = "home";
		}

		
		
		$this->loadTemplate($folder."/" . $file,$dados);

		
	}
	public function loadTemplate($viewName, $dados = array())
	{	
		require '../views/template.php';
	}

	public function loadViewIntemplate($viewName, $dados = array())
	{
		extract($dados);
		require '../views/'.$viewName.'.php';
	}

	private function responseAsJson($msg){
		return json_encode($msg);
	}
}


 ?>